# Astronaut iOS app


## Getting started

Building requires Xcode 12.1 or later, which requires an Intel-based Mac machine running macOS Catalina 10.15.4 or greater. 

## Architecture

The app uses "MVVM+Repository" architecture.

![MVVM+Repository](./Diagrams/AppArchitecture.png)

## Dependencies

Tool dependencies include:

1. Xcode (from Apple)
2. cocoapods (installed via gem)

Library dependencies include:

1. SDWebImage

## Building

1. Go to Project Root folder (../Astronaut/) in Terminal.
2. Run `Pod install` to install the dependency libraries.
3.  Open project by double clicking `Astronaut.xcworkspace`
4. Choose a simulator in Xcode eg. iPhone SE.
5. Click `Run` button to build and Run the project.

## Unit & UI Test

1. Build the project for Testing by choosing in menu `Product` > `Build For` > `Testing`
2. Run both Unit and UI by choosing in menu  `Product` > `Test`.


## Screenshots

| Home Screen | Sorted Home Screen | Bio Screen | 
|--|--|--|
|  ![HomeScreen](./Diagrams/HomeScreen.png) |  ![HomeScreenSorted](./Diagrams/HomeScreenSorted.png)|![BioScreen](./Diagrams/BioScreen.png) |
