//
//  AstronautListModelTest.swift
//  AstronautTests
//
//  Created by Vignesh Raj Somu on 13/5/21.
//

import XCTest
@testable import Astronaut

class AstronautListModelTest: XCTestCase {
    var response: Astronauts!
    
    override func setUp() {
        super.setUp()
        response = Astronauts.response
    }
    
    override func tearDown() {
        response = nil
    }
    
    func test_shouldDecodeJSON() {
        XCTAssertEqual(response.results.count, 10)
    }
    
    func test_shouldDecodeAstronaut() {
        let firstAstronaut = response.results.first
        
        XCTAssertEqual(firstAstronaut?.name, "Franz Viehböck")
        XCTAssertEqual(firstAstronaut?.birthDate, "1960-08-24")
        XCTAssertEqual(firstAstronaut?.nationality, "Austrian")
        XCTAssertEqual(firstAstronaut?.bio, "Franz Artur Viehböck (born August 24, 1960 in Vienna) is an Austrian electrical engineer, and was Austria's first cosmonaut. He was titulated „Austronaut“ by his country's media. He visited the Mir space station in 1991 aboard Soyuz TM-13, returning aboard Soyuz TM-12 after spending just over a week in space.")
        XCTAssertEqual(firstAstronaut?.profileImage, "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/default/cache/54/57/5457ce75acb7b188196eb442e3f17b64.jpg")

    }
}

private extension Astronauts {
    static let response: Astronauts = {
        return try! JSONTestHelper.responseFromJSONFile(
            withRelativePath: "JSONFiles/AstronautsResponse.json"
        )
    }()
}
