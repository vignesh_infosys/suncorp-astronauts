//
//  AstronautBioViewModelTests.swift
//  AstronautTests
//
//  Created by Vignesh Raj Somu on 13/5/21.
//

import Foundation
import XCTest
@testable import Astronaut

class AstronautBioViewModelTests: XCTestCase {
    var viewModel: AstronautBioViewModel!
    var mockRepository = MockDataRepository()
    
    override func setUp() {
        super.setUp()

        viewModel = AstronautBioViewModel(
            dataRepository: mockRepository,
            identifier: 1
        )
    }
    
    override func tearDown() {
        viewModel = nil
    }
    
    func test_WHEN_fetchAstronaut_isCalled_THEN_data_is_not_nil() {
        viewModel.fetchAstronaut()
        
        XCTAssertNotNil(viewModel.astronaut)
    }
}
