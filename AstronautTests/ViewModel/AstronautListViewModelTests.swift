//
//  AstronautListViewModelTests.swift
//  AstronautTests
//
//  Created by Vignesh Raj Somu on 13/5/21.
//

import Foundation
import XCTest
@testable import Astronaut

class AstronautListViewModelTests: XCTestCase {
    var viewModel: AstronautListViewModel!
    var mockRepository = MockDataRepository()
    
    override func setUp() {
        super.setUp()
        let closures = AstronautListViewModelClosures(showAstronaut: { _ in })

        viewModel = AstronautListViewModel(
            dataRepository: mockRepository,
            closures: closures)
    }
    
    override func tearDown() {
        viewModel = nil
    }
    
    func test_WHEN_syncAstronaut_isCalled_THEN_data_count_is_not_empty() {
        viewModel.syncAstronauts()
        
        XCTAssertEqual(viewModel.astronauts.count, 2)
    }
}
