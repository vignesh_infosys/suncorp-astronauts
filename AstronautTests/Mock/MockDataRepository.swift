//
//  MockDataRepository.swift
//  AstronautTests
//
//  Created by Vignesh Raj Somu on 13/5/21.
//

import Foundation
@testable import Astronaut

final class MockDataRepository: DataRepository {
    let mockAstronaut1 = Astronaut(id: 1,
                                  name: "Franz Viehböck",
                                  nationality: "Austrian",
                                  bio: "Franz Artur Viehböck (born August 24, 1960 in Vienna) is an Austrian electrical engineer, and was Austria's first cosmonaut. He was titulated „Austronaut“ by his country's media. He visited the Mir space station in 1991 aboard Soyuz TM-13, returning aboard Soyuz TM-12 after spending just over a week in space.",
                                  birthDate: "1960-08-24",
                                  profileImage: "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/default/cache/54/57/5457ce75acb7b188196eb442e3f17b64.jpg")
    
    let mockAstronaut2 = Astronaut(id: 2,
                                  name: "Marcos Pontes",
                                  nationality: "Brazilian",
                                  bio: "Marcos Cesar Pontes (born March 11, 1963) is a Brazilian Air Force pilot, engineer, AEB astronaut and author. He became the first South American and the first Lusophone to go into space when he launched into the International Space Station aboard Soyuz TMA-8 on March 30, 2006. He is the only Brazilian to have completed the NASA astronaut training program, although he switched to training in Russia after NASA's Space Shuttle program encountered problems.",
                                  birthDate: "1963-03-11",
                                  profileImage: "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/default/cache/b5/9b/b59bb16a31087708ffb212d3e6938946.jpg")
    
    func getAllAstronauts(completion: @escaping (Result<Astronauts, Error>) -> Void) {
        completion(.success(Astronauts(results: [mockAstronaut2, mockAstronaut1])))
    }
    
    func getAstronaut(withId identifier: String, completion: @escaping (Result<Astronaut, Error>) -> Void) {
        completion(.success(mockAstronaut1))
    }
}
