//
//  JSONTestHelper.swift
//  AstronautTests
//
//  Created by Vignesh Raj Somu on 13/5/21.
//

import Foundation

enum JSONTestHelper {
    /// Throwable generic function that returns a response of type `T` (where `T` is Decodable) from a given JSON file with `filename`.
    /// Because of the way the file URL is constructed, the function requires that the JSON file specify a file path relative to the FlybuysModelTests folder.
    static func responseFromJSONFile<T>(withRelativePath filepath: String) throws -> T where T: Decodable {
        let jsonURL = URL(fileURLWithPath: #file)
                .deletingLastPathComponent()
                .deletingLastPathComponent()
                .appendingPathComponent(filepath)
        let data = try Data.init(contentsOf: jsonURL)
        return try JSONDecoder().decode(T.self, from: data)
    }
}
