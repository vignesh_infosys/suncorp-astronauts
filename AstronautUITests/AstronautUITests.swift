//
//  AstronautUITests.swift
//  AstronautUITests
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import XCTest

class AstronautUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAstronautsListView() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        let tablesQuery = app.tables
        let tablesElement = tablesQuery.element

        waitForElementToAppear(element: tablesElement, timeout: 5)
        XCTAssertEqual(tablesQuery.cells.count, 10, "There should be 10 rows initially")
        
        tablesQuery.cells.containing(.staticText, identifier:"Jennifer Sidey-Gibbons").staticTexts["Nationality: Canadian"].tap()
        
        let astronautsButton = app.navigationBars["Astronaut.AstronautBioView"]
        XCTAssertEqual(astronautsButton.exists, true)
    }
    
    func waitForElementToAppear(element: XCUIElement, timeout: TimeInterval = 5,  file: String = #file, line: UInt = #line) {
            let existsPredicate = NSPredicate(format: "exists == true")

        expectation(for: existsPredicate,
                    evaluatedWith: element, handler: nil)

        waitForExpectations(timeout: timeout) { (error) -> Void in
                if (error != nil) {
                    let message = "Failed to find \(element) after \(timeout) seconds."
                    self.recordFailure(withDescription: message, inFile: file, atLine: Int(line), expected: true)
                }
            }
        }
}
