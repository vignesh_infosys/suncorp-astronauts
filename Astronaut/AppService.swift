//
//  AppService.swift
//  Suncorp
//
//  Created by Vignesh Raj Somu on 9/5/21.
//

import Foundation

final class AppService {
    static let shared = AppService()
    
    private let remoteDataSource: DataSource
    private let dataRepository: DataRepository!
    
    private init() {
        remoteDataSource = DefaultRemoteDataSource()
        dataRepository = DefaultDataRepository(remoteDataSource: remoteDataSource)
    }
    
    func getDataRepository() -> DataRepository {
        return dataRepository
    }
}
