//
//  DefaultDataRepository.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

final class DefaultDataRepository: DataRepository {
    private let remoteDataSource: DataSource
    private let localDataSource: DataSource?
    
    init(remoteDataSource: DataSource, localDataSource: DataSource? = nil) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }
    
    // Mark:- DataRepository

    func getAllAstronauts(completion: @escaping (Result<Astronauts, Error>) -> Void) {
        guard let localDatasource = localDataSource else {
            return  remoteDataSource.getAllAstronauts(completion: completion)
        }
        
        return localDatasource.getAllAstronauts(completion: completion)
    }
    
    func getAstronaut(withId identifier: String, completion: @escaping (Result<Astronaut, Error>) -> Void) {
        guard let localDatasource = localDataSource else {
            return  remoteDataSource.getAstronaut(withId: identifier, completion: completion)
        }
        
        return localDatasource.getAstronaut(withId: identifier, completion: completion)
    }
}
