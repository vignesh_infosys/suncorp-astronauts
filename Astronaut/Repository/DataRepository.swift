//
//  DataRepository.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

protocol DataRepository {
    func getAllAstronauts(completion: @escaping (Result<Astronauts, Error>) -> Void)
    func getAstronaut(withId identifier: String, completion: @escaping (Result<Astronaut, Error>) -> Void)
}
