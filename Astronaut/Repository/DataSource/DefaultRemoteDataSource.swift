//
//  DefaultRemoteDataSource.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

final class DefaultRemoteDataSource: DataSource {
    
    private let apiService: APIServicing
    
    init(apiService: APIServicing = DefaultAPIService(baseURL: "http://spacelaunchnow.me/api/")) {
        self.apiService = apiService
    }
    
    // MARK: - DataSource
    
    func getAllAstronauts(completion: @escaping (Result<Astronauts, Error>) -> Void) {
        return apiService.request(path: "3.5.0/astronaut", completion: {
            (response: Result<Astronauts, Error>)  in
            completion(response)
        })
    }
    
    func getAstronaut(withId identifier: String, completion: @escaping (Result<Astronaut, Error>) -> Void) {
        return apiService.request(path: "3.5.0/astronaut/\(identifier)", completion: {
            (response: Result<Astronaut, Error>)  in
            completion(response)
        })
    }
}
