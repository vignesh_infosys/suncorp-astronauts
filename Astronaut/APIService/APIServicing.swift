//
//  APIServicing.swift
//  Suncorp
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

protocol APIServicing {
    func request<T: Decodable>(path: String, completion: @escaping (Result<T, Error>) -> Void)
}
