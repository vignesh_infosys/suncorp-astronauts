//
//  DefaultAPIService.swift
//  Suncorp
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

final class DefaultAPIService: APIServicing {
    private let session = URLSession.shared
    private let jsonDecoder = JSONDecoder()
    private let baseURL: String!
    
    init(baseURL: String) {
        self.baseURL = baseURL
    }
    
    func request<T>(path: String, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable {
        guard let url = URL(string: baseURL + path) else { return }
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else {
                let error = NSError(domain: "com.network.service", code: 100, userInfo: nil)
                return completion(.failure(error))
            }
            
            self.session.dataTask(with: url, completionHandler: { data, response, error in
                
                if let requestError = error  {
                    return completion(.failure(requestError))
                } else {
                    guard let data = data else {
                        let error = NSError(domain: "com.network.service", code: 101, userInfo: nil)
                        return completion(.failure(error))
                    }
                    
                    do {
                        let decodedData = try self.jsonDecoder.decode(T.self, from: data)
                        return completion(.success(decodedData))
                    } catch {
                        let error = NSError(domain: "com.jsonparsing.data", code: 102, userInfo: nil)
                        return completion(.failure(error))
                    }
                }
            }).resume()
        }
    }
}
