//
//  Astronauts.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

struct Astronauts: Decodable {
    let results: [Astronaut]
}
