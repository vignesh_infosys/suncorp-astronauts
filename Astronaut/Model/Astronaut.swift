//
//  Astronaut.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

struct Astronaut: Decodable {
    let id: Int
    let name: String
    let nationality: String
    let bio: String
    let birthDate: String
    let profileImage: String
    
    enum CodingKeys: String, CodingKey {
        case id, name, nationality, bio
        case birthDate = "date_of_birth"
        case profileImage = "profile_image_thumbnail"
    }
}
