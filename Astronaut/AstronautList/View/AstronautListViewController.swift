//
//  AstronautListViewController.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import UIKit

class AstronautListViewController: UIViewController {

    @IBOutlet weak var errorButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var astronautsTableView: UITableView!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    
    private var astronautListViewModel: AstronautListViewModel!
    private var dataSource: AstronautsTableViewDataSource<AstronautsTableViewCell, Astronaut>!
    private var delegate: AstronautsTableViewDelegate!
    
    static func create(with viewModel: AstronautListViewModel) -> AstronautListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let astronautListScreen = storyboard.instantiateInitialViewController() as? AstronautListViewController else {
            fatalError("Cannot Instantiate initial view controller")
        }
        
        astronautListScreen.astronautListViewModel = viewModel
        astronautListScreen.delegate = AstronautsTableViewDelegate(viewModel: viewModel)
        
        return astronautListScreen
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        bindTo()
    }
    
    private func setupView() {
        activityIndicator.startAnimating()
        
        let nib = UINib(nibName: AstronautsTableViewCell.identifier, bundle: Bundle.main)
        self.astronautsTableView.register(nib, forCellReuseIdentifier: AstronautsTableViewCell.identifier)
        self.astronautsTableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        self.astronautsTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        self.sortButton.isEnabled = false
    }
    
    private func bindTo() {
        astronautListViewModel.bindViewModelToController = { [weak self] in
            DispatchQueue.main.async {
                self?.updateDataSource()
            }
        }
        
        astronautListViewModel.bindErrorToController = { [weak self] in
            self?.showError()
        }
        
        astronautListViewModel.syncAstronauts()
    }
    
    private func updateDataSource() {
        self.dataSource = AstronautsTableViewDataSource(
            cellIdentifier: AstronautsTableViewCell.identifier,
            items: self.astronautListViewModel.astronauts,
            configureCell: { (cell, astronaut) in
                cell.astronaut = astronaut
            })
                
        DispatchQueue.main.async {
            self.errorButton.isHidden = true
            self.astronautsTableView.isHidden = false
            self.sortButton.isEnabled = true
            self.astronautsTableView.dataSource = self.dataSource
            self.astronautsTableView.delegate = self.delegate
            self.astronautsTableView.refreshControl?.endRefreshing()
            self.astronautsTableView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
    
    private func showError() {
        DispatchQueue.main.async {
            self.errorButton.setTitle(NSLocalizedString("Network Error", comment: "Error"), for: .normal)
            self.errorButton.isHidden = false
            self.astronautsTableView.isHidden = true
            self.sortButton.isEnabled = false

            self.activityIndicator.stopAnimating()
        }
    }
    
    @objc private func refresh() {
        astronautListViewModel.syncAstronauts()
    }
    
    // MARK:- Target/Actions
    
    @IBAction func errorButtonTapped(_ sender: Any) {
        self.errorButton.isHidden = true
        self.activityIndicator.startAnimating()

        astronautListViewModel.syncAstronauts()
    }

    @IBAction func sortButtonTapped(_ sender: Any) {
        if astronautListViewModel.isAstronautSorted {
            astronautListViewModel.unsortAstronauts()
            self.sortButton.title = "Sort"
        } else {
            astronautListViewModel.sortAstronauts()
            self.sortButton.title = "Unsort"
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
