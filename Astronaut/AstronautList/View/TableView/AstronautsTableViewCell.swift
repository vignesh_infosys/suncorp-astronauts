//
//  AstronautsTableViewCell.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import UIKit
import SDWebImage

class AstronautsTableViewCell: UITableViewCell {

    static let identifier = "AstronautsTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!

    var astronaut : Astronaut? {
        didSet {
            guard  let astronaut = astronaut else { return }
            nameLabel.text = astronaut.name
            nationalityLabel.text = .nationality(country: astronaut.nationality)
            profileImage.sd_setImage(with: URL(string: astronaut.profileImage), completed: nil)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

private extension String {
    static func nationality(country: String) -> String {
        let localized = NSLocalizedString("Nationality", comment: "")
        return "\(localized) \(country)"
    }
}
