//
//  AstronautsTableViewDelegate.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation
import UIKit

final class AstronautsTableViewDelegate: NSObject, UITableViewDelegate {
    private var viewModel: AstronautListViewModel!

    init(viewModel: AstronautListViewModel) {
        self.viewModel = viewModel
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(index: indexPath.row)
    }
}
