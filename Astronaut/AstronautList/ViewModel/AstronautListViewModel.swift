//
//  AstronautListViewModel.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

struct AstronautListViewModelClosures {
    let showAstronaut: (Int) -> Void
}

final class AstronautListViewModel {
    private let dataRepository : DataRepository
    private let closures: AstronautListViewModelClosures
    
    private var unsortedAstronauts : [Astronaut] = [] {
        didSet {
            self.astronauts = unsortedAstronauts
        }
    }
    
    private(set) var astronauts : [Astronaut]! {
        didSet {
            self.bindViewModelToController()
        }
    }
    
    private(set) var error : Error? {
        didSet {
            self.bindErrorToController()
        }
    }
    
    var bindViewModelToController : (() -> ()) = {}
    var bindErrorToController : (() -> ()) = {}
    var isAstronautSorted: Bool = false
    
    init(dataRepository: DataRepository, closures: AstronautListViewModelClosures) {
        self.dataRepository = dataRepository
        self.closures = closures
    }
    
    func sortAstronauts() {
        self.astronauts = self.unsortedAstronauts.sorted { $0.name < $1.name }
        isAstronautSorted = true
    }
    
    func unsortAstronauts() {
        self.astronauts = self.unsortedAstronauts
        isAstronautSorted = false
    }
    
    func didSelect(index: Int) {
        let astronautIdentifier = self.unsortedAstronauts[index].id
        
        self.closures.showAstronaut(astronautIdentifier)
    }
    
    func syncAstronauts() {
        self.dataRepository.getAllAstronauts(completion: { [weak self] result in
            switch result {
            case .success(let astronauts):
                self?.unsortedAstronauts = astronauts.results
            case .failure(let error):
                self?.error = error
                break
            }
        })
    }
}
