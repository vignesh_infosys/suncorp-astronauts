//
//  AstronautBioViewModel.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import Foundation

final class AstronautBioViewModel {
    private let dataRepository : DataRepository
    private let identifier: Int
    
    private(set) var astronaut : Astronaut? {
        didSet {
            self.bindViewModelToController()
        }
    }
    
    private(set) var error : Error? {
        didSet {
            self.bindErrorToController()
        }
    }
    
    var bindViewModelToController : (() -> ()) = {}
    var bindErrorToController : (() -> ()) = {}
    var isAstronautSorted: Bool = false
    
    init(dataRepository: DataRepository, identifier: Int) {
        self.dataRepository = dataRepository
        self.identifier = identifier
    }
    
    func fetchAstronaut() {
        self.dataRepository.getAstronaut(withId: "\(identifier)", completion: { [weak self] result in
        switch result {
            case .success(let astronaut):
                self?.astronaut = astronaut
            case .failure(let error):
                self?.error = error
                break
            }
        })
    }
}
