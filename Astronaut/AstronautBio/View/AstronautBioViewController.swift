//
//  AstronautBioViewController.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import UIKit

class AstronautBioViewController: UIViewController {
    
    @IBOutlet weak var rootStackView: UIStackView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var errorButton: UIButton!
    @IBOutlet weak var profileView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private var astronautBioViewModel: AstronautBioViewModel!

    static func create(with viewModel: AstronautBioViewModel) -> AstronautBioViewController {
        let storyboard = UIStoryboard(name: "AstronautBioViewController", bundle: nil)
        guard let astronautBioScreen = storyboard.instantiateInitialViewController() as? AstronautBioViewController else {
            fatalError("Cannot Instantiate initial view controller")
        }
        
        astronautBioScreen.astronautBioViewModel = viewModel
        
        return astronautBioScreen
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindTo()
    }
    
    private func bindTo() {
        astronautBioViewModel.bindViewModelToController = { [weak self] in
            DispatchQueue.main.async {
                self?.updateView()
            }
        }
        
        astronautBioViewModel.bindErrorToController = { [weak self] in
            self?.showError()
        }
        
        astronautBioViewModel.fetchAstronaut()
        activityIndicator.startAnimating()
    }
    
    private func updateView() {
        guard let astronaut = astronautBioViewModel.astronaut else { return }
        
        self.errorButton.isHidden = true
        self.rootStackView.isHidden = false
        activityIndicator.stopAnimating()

        self.nameLabel.text = astronaut.name
        self.ageLabel.text = .birthDate(date: astronaut.birthDate)
        self.nationalityLabel.text = .nationality(country: astronaut.nationality)
        self.bioTextView.text = astronaut.bio
        self.profileView.sd_setImage(with: URL(string: astronaut.profileImage), completed: nil)
    }
    
    private func showError() {
        DispatchQueue.main.async {
            self.errorButton.setTitle(NSLocalizedString("Network Error", comment: "Error"), for: .normal)
            self.errorButton.isHidden = false
            self.rootStackView.isHidden = true

            self.activityIndicator.stopAnimating()
        }
    }
    
    // MARK:- Target/Actions
    
    @IBAction func errorButtonTapped(_ sender: Any) {
        self.errorButton.isHidden = true
        self.activityIndicator.startAnimating()

        astronautBioViewModel.fetchAstronaut()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

private extension String {
    static func nationality(country: String) -> String {
        let localized = NSLocalizedString("Nationality", comment: "")
        return "\(localized) \(country)"
    }
    
    static func birthDate(date: String) -> String {
        let localized = NSLocalizedString("DOB", comment: "")
        return "\(localized) \(date)"
    }
}
