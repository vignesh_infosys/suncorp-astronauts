//
//  AppFlowCoordinator.swift
//  Astronaut
//
//  Created by Vignesh Raj Somu on 12/5/21.
//

import UIKit

final class AppFlowCoordinator {
    private weak var window: UIWindow?
    private let dataRepository = AppService.shared.getDataRepository()
    private let navigationController = UINavigationController()
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let viewMoel = createAstronautListViewModel()
        let viewController = AstronautListViewController.create(with: viewMoel)
        
        navigationController.setViewControllers([viewController], animated: false)
        
        self.window?.rootViewController = navigationController
    }
    
    private func createAstronautListViewModel() -> AstronautListViewModel{
        let closures = AstronautListViewModelClosures(showAstronaut: showAstronaut)
        return AstronautListViewModel(dataRepository: dataRepository, closures: closures)
    }
    
    private func createAstronautBioViewModel(identifier: Int) -> AstronautBioViewModel{
        return AstronautBioViewModel(dataRepository: dataRepository, identifier: identifier)
    }
    
    private func showAstronaut(identifier: Int) {
        let viewMoel = createAstronautBioViewModel(identifier: identifier)
        let viewController = AstronautBioViewController.create(with: viewMoel)
        
        navigationController.pushViewController(viewController, animated: true)
    }
}
